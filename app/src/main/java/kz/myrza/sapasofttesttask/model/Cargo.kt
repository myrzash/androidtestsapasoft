package kz.myrza.sapasofttesttask.model

data class Cargo(
    var cargoName: String = "",
    var startDate: String = "",
    var endDate: String = "",
    var weight: Double = 0.0,
    var volume: Double = 0.0,
    var loadLocations: List<Location>,
    var unloadLocations: List<Location>
)