package kz.myrza.sapasofttesttask.controller

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kz.myrza.sapasofttesttask.model.Cargo
import kz.myrza.sapasofttesttask.api.CargoService
import java.util.concurrent.TimeUnit

class CargoController {

    private var service: CargoService
    private var view: CargoView

    constructor(view: CargoView) {
        this.view = view
        this.service = CargoService()
        fetchCargoList()
    }

    private fun fetchCargoList() {
        service.getCargos()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view.setValues(it)
                }, {
                    view.onError()
                }
            )
    }

    private fun addValues(pageNumber: Int = 0) {
        service.getCargos(pageNumber, 10)
            .subscribeOn(Schedulers.newThread())
            .delay(800, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    view.addValues(it)
                }
            )
    }

    fun onRefresh() {
        this.fetchCargoList()
    }

    fun loadMoreItems(page: Int = 0) {
        this.addValues(page)
    }

    interface CargoView {
        var isLastPage: Boolean
        fun setValues(cargoList: List<Cargo>)
        fun addValues(cargoList: List<Cargo>)
        fun onError()
    }
}