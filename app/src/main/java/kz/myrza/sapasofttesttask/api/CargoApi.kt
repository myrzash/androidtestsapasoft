package kz.myrza.sapasofttesttask.api

import io.reactivex.Observable
import kz.myrza.sapasofttesttask.model.Cargo
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface CargoApi {

    @POST("cargo-applications-filter")
    fun getCargos(
        @Query("page") page: Int = 0,
        @Query("size") size: Int = 0,
        @Body requestBody: CargoRequestBody = CargoRequestBody()
    ): Observable<List<Cargo>>

}