package kz.myrza.sapasofttesttask.api

import io.reactivex.Observable
import kz.myrza.sapasofttesttask.model.Cargo
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class CargoService {
    private var api: CargoApi

    companion object {
        val BASE_URL = "http://ecargo.sapasoft.kz/api/"
    }

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        api = retrofit.create(CargoApi::class.java)
    }

    fun getCargos(page: Int  = 0, size: Int = 0): Observable<List<Cargo>> {
        return api.getCargos(page, size)
    }
}