package kz.myrza.sapasofttesttask.pagination

import android.nfc.tech.MifareUltralight.PAGE_SIZE
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView


class PaginationListener : RecyclerView.OnScrollListener {

    private var view: PaginationView
    private var layoutManager: LinearLayoutManager

    constructor (view: PaginationView, layoutManager: LinearLayoutManager) : super() {
        this.view = view
        this.layoutManager = layoutManager
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val visibleItemCount = layoutManager.getChildCount()
        val totalItemCount = layoutManager.getItemCount()
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
        if (!view.isLoading && !view.isLastPage) {
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= PAGE_SIZE
            ) {
                view.loadMoreItems()
            }
        }
    }

    interface PaginationView {
        var isLastPage: Boolean
        var isLoading: Boolean
        fun loadMoreItems()
    }

}