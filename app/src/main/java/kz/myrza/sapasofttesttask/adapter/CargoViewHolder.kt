package kz.myrza.sapasofttesttask.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.View
import kz.myrza.sapasofttesttask.databinding.ItemListCargoBinding
import kz.myrza.sapasofttesttask.model.Cargo

class CargoViewHolder : RecyclerView.ViewHolder {

    var binding: ItemListCargoBinding

    constructor (view: View) : super(view) {
       binding = DataBindingUtil.bind<ItemListCargoBinding>(view)!!
    }

    fun bind (cargo: Cargo) {
        this.binding.cargo = cargo
    }
}