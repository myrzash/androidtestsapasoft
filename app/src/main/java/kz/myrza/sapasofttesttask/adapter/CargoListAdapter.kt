package kz.myrza.sapasofttesttask.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import kz.myrza.sapasofttesttask.databinding.ItemListCargoBinding
import kz.myrza.sapasofttesttask.model.Cargo


class CargoListAdapter(val cargoList: ArrayList<Cargo>, val context: Context) : RecyclerView.Adapter<CargoViewHolder>() {

    var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CargoViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemListCargoBinding.inflate(inflater, parent, false)
        return CargoViewHolder(binding.root)
    }

    override fun getItemCount(): Int = cargoList.size

    override fun onBindViewHolder(cargoViewHolder: CargoViewHolder, position: Int) {
        val cargo = cargoList.get(position)
        setAnimation(cargoViewHolder.itemView, position)
        cargoViewHolder.bind(cargo)
    }

    /**
     * Here is the key method to apply the animation
     */
    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }
}