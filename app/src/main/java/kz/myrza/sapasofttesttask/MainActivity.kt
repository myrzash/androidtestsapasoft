package kz.myrza.sapasofttesttask

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kz.myrza.sapasofttesttask.adapter.CargoListAdapter
import kz.myrza.sapasofttesttask.controller.CargoController
import kz.myrza.sapasofttesttask.model.Cargo
import kz.myrza.sapasofttesttask.pagination.PaginationListener
import kz.myrza.sapasofttesttask.MainActivity as MainActivity1

@SuppressLint("RestrictedApi")
class MainActivity : AppCompatActivity(), CargoController.CargoView, PaginationListener.PaginationView {

    private lateinit var presenter: CargoController
    private var cargoList = ArrayList<Cargo>()
    private lateinit var cargoListAdapter: CargoListAdapter

    override var isLastPage: Boolean = false
    override var isLoading: Boolean = false
    var pageNumber = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = CargoController(this)
        cargoListAdapter = CargoListAdapter(cargoList, this)

        val linearLayoutManager = LinearLayoutManager(applicationContext)
        recyclerViewMain.apply {
            layoutManager = linearLayoutManager
            adapter = cargoListAdapter
        }
        recyclerViewMain.addOnScrollListener(PaginationListener(this, linearLayoutManager))


        fabRetry.setOnClickListener(View.OnClickListener {
            presenter.onRefresh()

            progressBarCargoList.visibility = View.VISIBLE
            fabRetry.visibility = View.GONE
            recyclerViewMain.visibility = View.GONE
        })
    }

    override fun onError() {
        Toast.makeText(this, R.string.error_connection, Toast.LENGTH_SHORT).show()

        progressBarCargoList.visibility = View.GONE
        fabRetry.visibility = View.VISIBLE
        recyclerViewMain.visibility = View.GONE
    }

    override fun setValues(cargoList: List<Cargo>) {
        this.cargoList.clear()
        this.cargoList.addAll(cargoList)
        cargoListAdapter.notifyDataSetChanged()

        progressBarCargoList.visibility = View.GONE
        fabRetry.visibility = View.GONE
        recyclerViewMain.visibility = View.VISIBLE
    }

    override fun addValues(cargoList: List<Cargo>) {

        progressBarLoadMore.visibility = View.GONE

        if(cargoList.isEmpty()) {
            isLastPage = true
        } else {
            this.cargoList.addAll(cargoList)
            cargoListAdapter.notifyDataSetChanged()
            isLoading = false
        }
    }

    override fun loadMoreItems() {
        presenter.loadMoreItems(++pageNumber)
        isLoading = true
        progressBarLoadMore.visibility = View.VISIBLE
        Log.d("LOG_TAG", "loadMoreItems...")
    }
}
